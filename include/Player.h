#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "common.h"
#include "Worm.h"

class Player
{
public:
  string name;
  vector<Worm> worms;
  char wormIcon;

  int numWorms();

  Player();
  Player(string name, char wormIcon);
};

#endif