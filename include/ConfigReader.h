#ifndef __CONFIG_READER_H__
#define __CONFIG_READER_H__

#include "common.h"
#include "MapElement.h"
#include "State.h"

class ConfigReader
{
  public:
    State& state;
    void readGameConfig(string filename);
    vector<vector<MapElement>> readMapConfig(string filename);

    ConfigReader(State&);
};

#endif