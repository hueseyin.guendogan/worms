#ifndef __STATE_H__
#define __STATE_H__

#include "common.h"
#include "Player.h"
#include "Worm.h"
#include "MapElement.h"

class State
{
public:
  string curentPlayer;
  Worm selectedWorm;
  string selectedWeapon;
  vector<Player> allPlayers;
  vector<string> allWeapons;
  vector<vector<MapElement>> map;
  int numWorms;
  int numPlayers;
  bool gameEnd;

  void printMap() {
    if (map.size() == 0) return;

    const int x = map[0].size();
    const int y = map.size();

    for (int i = 0; i < y; i++)
    {
      for (int k = 0; k < x; k++)
      {
        cout << map[i][k].value;
      }
      cout << endl;      
    }
    
  }

  State();
};

#endif