#ifndef __WORM_H__
#define __WORM_H__

#include "common.h"
#include "Position.h"
#include "Weapon.h"

class Worm
{
public:
  string name;
  char icon;
  int8_t health;
  vector<Weapon> weapons;
  Position position;
  
  Worm();
  Worm(string name);
};

#endif