#ifndef __DAMAGE_RADIUS_H__
#define __DAMAGE_RADIUS_H__

#include "common.h"

enum Scope {
  // CIRCLE,
  LINE,
  CROSS
};

class DamageRadius
{
public:
  int radius;
  Scope scope;

  DamageRadius();
  DamageRadius(int radius, Scope scope);
};

#endif