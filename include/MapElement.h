#ifndef __MAP_ELEMENT_H__
#define __MAP_ELEMENT_H__

#include "Position.h"

class MapElement
{
public:
  char value;
  Position position;

  MapElement(char, Position);
};

#endif