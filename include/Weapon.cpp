#include "Weapon.h"
#include "DamageRadius.cpp"

Weapon::Weapon(){};

Weapon::Weapon(string name, int ammunition, int damage, vector<string> fireDirection, DamageRadius damageRadius) : name(name), ammunition(ammunition), fireDirection(fireDirection), damageRadius(damageRadius){};
