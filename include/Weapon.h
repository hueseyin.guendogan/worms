#ifndef __WEAPON_H__
#define __WEAPON_H__

#include "common.h"
#include "DamageRadius.h"

class Weapon
{
public:
  string name;
  int ammunition;
  int damage;
  vector<string> fireDirection;
  DamageRadius damageRadius;

  // define static methods for all weapons
  static Weapon getPistol() {
    Weapon *w = new Weapon();
    w->name = "Pistol";
    w->ammunition = -1;
    w->damage = 25;
    w->fireDirection = { "t", "r", "b", "l", "tr", "br", "bl", "tl" };
    w->damageRadius = *(new DamageRadius(5, Scope::LINE));
    return *w;
  }

  Weapon();
  Weapon(string name, int ammunition, int damage, vector<string> fireDirection, DamageRadius damageRadius);
};

#endif