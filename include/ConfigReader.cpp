#include <time.h>
#include "common.h"
#include "ConfigReader.h"
#include "MapElement.cpp"
#include "Position.cpp"

ConfigReader::ConfigReader(State &s) : state(s){};

/*
  This function splits a string by a given delimiter and returns a string array.
  ex:
    string: "MAP=some/path/to/Map"
    delimiter: '='
    result: [ "MAP", "some/path/to/Map" ]

  @param str String to split
  @param delimiter Delimiter character for splitting

  @return A pair of strings
*/
pair<string, string> splitString(string str, string delimiter)
{
  const string token = str.substr(0, str.find(delimiter));
  auto start = 0U;
  auto end = str.find(delimiter);

  string first;

  while (end != string::npos)
  {
    first = str.substr(start, end - start);
    start = end + delimiter.length();
    end = str.find(delimiter, start);
  }

  string last = str.substr(start, end);
  pair<string, string> result = make_pair(first, last);
  return result;
}

/*
  Checks, if a substring exists in a string.
  ex:
    str: "abcdef"
    substr: "bc"
    result: 1

    str: "abcdef"
    substr: "ca"
    result: 0

  @param str The string to check 
  @param substr The substring to check in str
  
  @return 1 if substr exists in str otherwise 0
 */
bool hasSubstring(string str, string substr)
{
  return str.find(substr) != std::string::npos;
}

/*
  Reads the map from a file and returns a two dimensional character vector.

  @param mapPath The path of the file, where the map is defined

  @return Two dimensional character vector, consisting the map elements
*/
vector<vector<MapElement>> ConfigReader::readMapConfig(string mapPath)
{
  fstream mapConfigFile;
  mapConfigFile.open(mapPath, ios::in); // open the file in mapPath with read mode (ios::in)
  vector<vector<MapElement>> map;

  // if the map file opened successfully
  if (mapConfigFile.is_open())
  {
    string line;
    int x = 0;
    int y = 0;

    // Read map file line by line. Store in each iteration
    // the content of current line into "line" variable
    while (getline(mapConfigFile, line))
    {
      x = line.size();
      y++;
    }

    // set the vector size
    map.resize(y);
    // set cursor to the begin of the file,
    // because the while loop above brought the cursor to the end of the file
    mapConfigFile.clear();
    mapConfigFile.seekg(0, ios::beg);

    int i = 0;

    // Read file line by line
    while (getline(mapConfigFile, line))
    {
      /*
        The line is a string, therefore iterate over it
        Put each character into the i'th line of the vector

        ex:
          line: "ABC"
          iteration 1: 
            line[k] = 'A'
            map[i].push_back('A')
            => map[i] = ['A']
          iteration 2: 
            line[k] = 'B'
            map[i].push_back('B')
            => map[i] = ['A', 'B']
          iteration 3:
            line[k] = 'C'
            map[i].push_back('C')
            => map[i] = ['A', 'B', 'C']
      */
      for (int k = 0; k < line.size(); k++)
      {
        // Create a position object
        Position *position = new Position(k, i);
        // Create a map element object with the character value line[k] (k'th element of line i) and the position above
        MapElement *mapElement = new MapElement(line[k], *position);
        map[i].push_back(*mapElement);
      }
      i++; // go to next line
    }
  }

  return map;
}

vector<string> readWormNames(string filePath)
{
  fstream file;
  file.open(filePath, ios::in); // open the file in filePath with read mode (ios::in)
  vector<string> names;

  // if the file is opened successfully
  if (file.is_open())
  {
    string line;
    while (getline(file, line))
    {
      names.push_back(line);
    }
  }
  file.close();
  return names;
}

void ConfigReader::readGameConfig(string filePath)
{
  fstream configFile;
  configFile.open(filePath, ios::in); // open the filePath with read mode (ios::in)

  // if the config file opened successfully
  if (configFile.is_open())
  {
    string line;
    vector<string> wormNames;

    while (getline(configFile, line))
    {

      // if the line start with '[' or '#' or ' ', then ignore this line and continue to next line
      const char lineStart = line[0];
      if (lineStart == '[' || lineStart == '#' || line.empty())
        continue;

      // split the string by '=' delimiter
      // return value is a pair ex: Pair("NUM_PLAYERS", "2")
      pair<string, string> pair = splitString(line, "=");

      if (pair.first == "MAP")
      {
        state.map = readMapConfig(pair.second);
      }

      if (pair.first == "WORM_NAMES")
      {
        const string wormNamesPath = pair.second;
        vector<string> _wormNames = readWormNames(wormNamesPath);
        wormNames.resize(_wormNames.size());
        wormNames = _wormNames;
      }
      if (pair.first == "NUM_WORMS")
      {
        srand(time(NULL)); // needed for random number

        int numWorms = stoi(pair.second);
        state.numWorms = numWorms;

        // Iterate over all players and initial worms with random names
        for (int i = 0; i < state.allPlayers.size(); i++)
        {
          state.allPlayers[i].worms.resize(numWorms);
          state.allPlayers[i].worms = {};
          cout << state.allPlayers[i].name << endl;

          // create worms with random names and insert them in player.worms vector
          for (int j = 0; j < numWorms; j++)
          {
            int randomWorm = rand() % wormNames.size();
            string wormName = wormNames[randomWorm];
            const Worm *w = new Worm(wormName);
            state.allPlayers[i].worms.push_back(*w);
          }
        }

      }
    }
    configFile.close(); //close the file object.
  }
}
// void readGameConfig(string filePath);