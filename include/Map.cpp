#include "common.h"
#include "MapElement.h"

typedef vector<vector<MapElement>> Map;

static Map initMap(Map m)
{
  const int x = m[0].size();
  const int y = m.size();

  for (int i = 0; i < y; i++)
  {
    for (int k = 0; k < x; k++)
    {
      const char value = m[i][k].value;

      if (value == 'A')
      {
        m[i][k].value = ' ';
      }
      if (value == 'W')
      {
        m[i][k].value = '-';
      }
    }
  }

  return m;
}