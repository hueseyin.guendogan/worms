#include <iostream>
#include <fstream>
#include "../include/Player.cpp"
#include "../include/ConfigReader.cpp"
#include "../include/State.cpp"

int main()
{
    string name;
    cout << "Type in Player 1 name: ";
    cin >> name;
    Player *player1 = new Player(name, '*');

    cout << "Type in Player 2 name: ";
    cin >> name;
    Player *player2 = new Player(name, '~');

    State *state = new State();
    state->allPlayers.push_back(*player1);
    state->allPlayers.push_back(*player2);

    ConfigReader *cr = new ConfigReader(*state);
    cr->readGameConfig("../assets/config.txt");

    cout << state->allPlayers[0].name << endl;
    cout << state->map[10][1].value << " ";
    cout << state->map[10][1].position.x << "/";
    cout << state->map[10][1].position.y << endl;

    return 0;
}